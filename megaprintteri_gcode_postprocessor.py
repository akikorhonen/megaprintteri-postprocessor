#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

#
# G-Code postprocessor for Megaprintteri
# AK 2016
#

#
# Jos puolipiste -> kommentti -> vaihda puolipiste suluiksi:
# ( KOMMENTTI )
#
# Koodin alkuun:
# - L�mp�tilan s��t�: M109 Pxxx -> M110 Pxxx
# - M190 Pxxx -> M111 Pxxx
# M104 Pxxx -> M110 Pxxx
# M140 Pxxx -> M111 Pxxx
#

# --------------------------------------------------------------------------- #

import os
import sys
import datetime

# --------------------------------------------------------------------------- #

def main():
	print "Megaprintteri G-code postprocessor v0.1"
	print ""
	print "Usage: script_name [gcode-file]"
	print ""
	
	arguments = sys.argv
	script_location = str(arguments[0])
	
	if(len(arguments) == 1):
		print "No input files, press enter to exit."
		raw_input()
		exit()
	
	input_file = str(arguments[1])
	file_path = os.path.dirname(input_file)
	
	file_name_full = os.path.basename(input_file)
	file_name = os.path.splitext(file_name_full)[0]
	file_extension = os.path.splitext(file_name_full)[1]
	
	timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
	
	output_file = file_path + "\\" + file_name + "_megaprintr_" + str(timestamp) + ".cnc"
	
	#print input_file
	#print file_path
	#print file_name_full
	#print file_name
	#print file_extension
	#print output_file
	
	status = parse_gcode(input_file, output_file)
	
	if(status == 0):
		print "Input: " + input_file
		print "Output: " + output_file
	else:
		print "Error " + str(status)
	
	print " --- "
	print ""
	print "Press enter to exit"
	raw_input()

# --------------------------------------------------------------------------- #

def parse_gcode(input_file, output_file):
	print "Opening file " + input_file
	with open(input_file, 'r') as f:
		data = f.readlines()
	
	data_out = ""
	
	# Remove \n and \r
	for i, line in enumerate(data):
		line = line.replace("\n", "")
		line = line.replace("\r", "")
		data[i] = line
	
	# IN
	#for line in data:
	#	print line
	
	print " --- "
	print ""
	
	comment_counter = 0
	linecomment_counter = 0
	
	for i, line in enumerate(data):
		if(0 < len(line)):
			if(line[0:5] == "M109 "):
				data[i] = line.replace("M109", "M110")
				print "> M109 changed to M110"
			elif(line[0:5] == "M190 "):
				data[i] = line.replace("M190", "M111")
				print "> M190 changed to M111"
			elif(line[0:5] == "M104 "):
				data[i] = line.replace("M104", "M110")
				print "> M104 changed to M110"
			elif(line[0:5] == "M140 "):
				data[i] = line.replace("M140", "M111")
				print "> M140 changed to M111"
		
		if(0 < len(line)):
			if(line[0] == ";"):
				data[i] = "(" + line[1:] + " )"
				comment_counter = comment_counter + 1
			elif(";" in line):
				line = line.replace(";", "(")
				data[i] = line + " )"
				linecomment_counter = linecomment_counter + 1
		
		data[i] = data[i] + "\n"
	
	print "> %d comment lines fixed" % comment_counter
	print "> %d line comments fixed" % linecomment_counter
	
	print ""
	print " --- "
	
	# OUT
	#for line in data:
	#	print line
	
	with open(output_file, 'w') as f:
		for line in data:
			f.write(line)
	
	return 0

# --------------------------------------------------------------------------- #

if __name__ == "__main__":
    main()

# --------------------------------------------------------------------------- #
